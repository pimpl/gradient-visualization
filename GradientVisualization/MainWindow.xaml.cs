﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using GradientVisualization.Model;
using System.Threading;
using System.ComponentModel;
using System.Windows.Threading;

namespace GradientVisualization
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initialize structures
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(MyHandler);
            functionComboBox.Items.Add("y = ax");
            functionComboBox.Items.Add("y = ax + b");
            functionComboBox.Items.Add("y = ax^b");
            functionComboBox.Items.Add("Sigmoid (y=1/1+e^-x)");
            this.mainChart.DisableAnimations = true;

            SeriesCollection SeriesCollection = new SeriesCollection {
             new LineSeries
             {
                     Values = new ChartValues<double> { 3, 5, 7, 100 }
             }};

            var test = new Axis();
            test.Labels = new string[] { "1", "2", "3", "4" };
            this.mainChart.AxisX.Add(test);
            this.mainChart.Series = SeriesCollection;
        }
        /// <summary>
        /// Handling exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MyHandler(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.ExceptionObject.ToString());
            mainChart.Series = null;
            PauseRunButton_Click(sender, null);
        }

        public double ticks = 0;
        public System.Windows.Threading.DispatcherTimer dispatcherTimer;

        /// <summary>
        /// Start learning with data from model
        /// </summary>
        /// <param name="model"></param>
        /// <param name="genMethod"></param>
        public void runLearning(VisualizationModel model, Action<VisualizationModel> genMethod)
        {
            try
            {
                Beta1TextBox.Text = model.beta1.ToString();
                var labels = new Axis();
                labels.Labels = model.elements.OrderBy(p => p.x).Select(p => p.x.ToString()).ToArray();
                var values = model.elements.OrderBy(p => p.x).Select(p => p.yPrim).ToArray();
                var actualValues = model.elements.OrderBy(p => p.x).Select(p => p.y).ToArray();
                SeriesCollection SeriesCollection = new SeriesCollection {
                new LineSeries
                {
                        Values = new ChartValues<double>(values)
                },
                new ScatterSeries
                {
                    Values = new ChartValues<double>(actualValues)
                }

            };
                this.mainChart.AxisX = new AxesCollection();
                this.mainChart.AxisX.Add(labels);

                var max = model.elements.Max(p => p.y);

                this.mainChart.AxisY[0].MaxValue = max;
                this.mainChart.AxisY[0].MinValue = 0;
                this.mainChart.Series = SeriesCollection;

                ticks = 0;
                dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                dispatcherTimer.Tick += (sender, e) => timer_Tick(sender, e, () =>
                {
                    genMethod(model);
                });
                dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, model.delay);
                dispatcherTimer.Start();
            }
            catch (Exception e)
            {
                PauseRunButton_Click(null, null);

            }
        }

        /// <summary>
        /// Periodically triggering function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="calculateEpoch"></param>
        private void timer_Tick(object sender, EventArgs e, Action calculateEpoch)
        {
            ticks++;
            if (ticks == model.epoch)
            {
                dispatcherTimer.Stop();
            }
            else
            {
                try
                {
                    calculateEpoch();
                    this.Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            if (double.IsNaN(model.beta1) || double.IsNaN(model.beta2))
                            {
                                throw new Exception("NaN value");
                            }
                            epochTextBlock.Text = ticks.ToString();
                            Beta1TextBox.Text = model.beta1.ToString();
                            Beta2TextBox.Text = model.beta2.ToString();
                            var values = model.elements.OrderBy(p => p.x).Select(p => p.yPrim).ToArray();
                            var item = mainChart.Series[0];
                            for (int k = 0; k < item.Values.Count; k++)
                            {
                                item.Values[k] = values[k];
                            }
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.Message);
                            PauseRunButton_Click(null, null);
                        }
                    });
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                    PauseRunButton_Click(null, null);
                }
            }
        }
        /// <summary>
        /// Visualization data model
        /// </summary>
        public VisualizationModel model;
        /// <summary>
        /// Generate method
        /// Initialize visualization model
        /// Choosing type of function
        /// Trigger runLearning() function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            epochTextBlock.Text = "0";
            if (dispatcherTimer != null)
            {
                dispatcherTimer.Stop();
            }
            model = new VisualizationModel();
            model.a = double.Parse(this.aTextBox.Text);

            model.b = double.Parse(this.bTextBox.Text);
            model.beta1 = double.Parse(this.Beta1TextBox.Text);
            model.beta2 = double.Parse(this.Beta2TextBox.Text);
            model.learn = double.Parse(this.LearnTextBox.Text);
            model.epoch = double.Parse(this.EpochTextBox.Text);
            model.delay = int.Parse(this.DelayTextBox.Text);
            model.rangeFrom = double.Parse(this.RangeFromTextBox.Text);
            model.rangeTo = double.Parse(this.RangeToTextBox.Text);

            switch (functionComboBox.SelectionBoxItem.ToString())
            {
                case "moja":
                    {
                        Random r = new Random();
                        Elem[] tabElem = new Elem[8];
                        double[] tabx = new double[8] { -2, -3, 1, 2, 3, 4, 7, 1.5 };
                        double[] taby = new double[8] {0,0,0,1,1,1,1,0.5 };
                        for (int i = 0; i < 8; i++)
                        {
                            tabElem[i] = new Model.Elem();
                            tabElem[i].x = tabx[i];
                            tabElem[i].y = taby[i];
                        }
                        model.elements = tabElem;
                        runLearning(model, (model) =>
                        {
                            for (int i = 0; i < model.elements.Count(); i++)
                            {
                                model.elements[i].yPrim = 1 /( 1 + Math.Pow(Math.E, (-1) * model.beta1 * model.elements[i].x - model.beta2));
                                model.beta1 = model.beta1 - (model.learn * (model.elements[i].yPrim - model.elements[i].y) * model.elements[i].yPrim) * (1 - model.elements[i].yPrim) * model.elements[i].x;
                                model.beta2 = model.beta2 - (model.learn * (model.elements[i].yPrim - model.elements[i].y) * model.elements[i].yPrim) * (1 - model.elements[i].yPrim);
                            }
                        });
                        break;
                    }
                case "y = ax":
                    {
                        Random r = new Random();
                        Elem[] tabElem = new Elem[10];

                        for (int i = 0; i < 10; i++)
                        {
                            tabElem[i] = new Model.Elem();
                            tabElem[i].x = i + 1;
                            tabElem[i].y = model.a * tabElem[i].x;
                        }
                        model.elements = tabElem;
                        runLearning(model, (model) =>
                        {
                            for (int i = 0; i < model.elements.Count(); i++)
                            {
                                model.elements[i].yPrim = model.beta1 * model.elements[i].x;
                                model.beta1 = model.beta1 - (model.elements[i].yPrim - model.elements[i].y) * model.elements[i].x * model.learn;
                            }
                        });
                        break;
                    }
                case "y = ax + b":
                    {
                        Random r = new Random();
                        Elem[] tabElem = new Elem[10];

                        for (int i = 0; i < 10; i++)
                        {
                            tabElem[i] = new Model.Elem();
                            //tabElem[i].x = r.Next((int)model.rangeFrom, (int)model.rangeFrom);
                            tabElem[i].x = i + 1;
                            tabElem[i].y = model.a * tabElem[i].x + model.b;
                        }
                        model.elements = tabElem;
                        runLearning(model, (model) =>
                        {
                            for (int i = 0; i < model.elements.Count(); i++)
                            {
                                model.elements[i].yPrim = model.elements[i].x * model.beta1 + model.beta2;
                                model.beta1 = model.beta1 - (model.elements[i].x * model.elements[i].yPrim - model.elements[i].x * model.elements[i].y) * model.learn;
                                model.beta2 = model.beta2 - (model.elements[i].yPrim - model.elements[i].y) * model.learn;
                            }
                        });
                        break;
                    }
                case "y = ax^b":
                    {
                        Random r = new Random();
                        Elem[] tabElem = new Elem[10];

                        for (int i = 0; i < 10; i++)
                        {
                            tabElem[i] = new Model.Elem();
                            tabElem[i].x = i + 1;
                            tabElem[i].y = (tabElem[i].x * tabElem[i].x) * 2;
                        }
                        model.elements = tabElem;
                        runLearning(model, (model) =>
                        {
                            for (int i = 0; i < model.elements.Count(); i++)
                            {
                                model.elements[i].yPrim = model.beta1 * Math.Pow(model.elements[i].x, model.beta2);
                                model.beta1 = model.beta1 - (tabElem[i].yPrim - model.elements[i].y) * model.learn * Math.Pow(model.elements[i].x, model.beta2);
                                model.beta2 = model.beta2 - (tabElem[i].yPrim - model.elements[i].y) * model.beta1 * (Math.Log(model.elements[i].x) * Math.Pow(tabElem[i].x, model.beta2)) * model.learn;
                            }
                        });
                        break;
                    }



                default:
                    break;
            }

        }
        /// <summary>
        /// Pause and run process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PauseRunButton_Click(object sender, RoutedEventArgs e)
        {
            if (dispatcherTimer != null)
            {
                if (dispatcherTimer.IsEnabled)
                {
                    dispatcherTimer.Stop();
                }
                else
                {
                    dispatcherTimer.Start();
                }
            }
        }
    }
}

﻿using LiveCharts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradientVisualization.Model
{
    /// <summary>
    /// Class reprezent Visualization Model
    /// </summary>
    public class VisualizationModel
    {
        public SeriesCollection SeriesCollection;

        private double _a;

        public double a
        {
            get { return _a; }
            set { _a = value; }
        }

        private double _b;

        public double b
        {
            get { return _b; }
            set { _b = value; }
        }

        private double _beta1;

        public double beta1
        {
            get { return _beta1; }
            set { _beta1 = value; }
        }

        private double _beta2;

        public double beta2
        {
            get { return _beta2; }
            set { _beta2 = value; }
        }

        private double _learn;

        public double learn
        {
            get { return _learn; }
            set { _learn = value; }
        }

        private double _epoch;

        public double epoch
        {
            get { return _epoch; }
            set { _epoch = value; }
        }

        private int _delay;

        public int delay
        {
            get { return _delay; }
            set { _delay = value; }
        }

        private double _rangeFrom;

        public double rangeFrom
        {
            get { return _rangeFrom; }
            set { _rangeFrom = value; }
        }

        private double _rangeTo;

        public double rangeTo
        {
            get { return _rangeTo; }
            set { _rangeTo = value; }
        }

        private Elem[] _elements;

        public Elem[] elements
        {
            get { return _elements; }
            set { _elements = value; }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradientVisualization.Model
{
    /// <summary>
    /// Class represent one survey
    /// </summary>
    public class Elem
    {
        public double x;
        public double y;
        public double yPrim;
    }
}
